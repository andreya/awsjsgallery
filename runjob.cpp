#include <windows.h> 
#include <stdio.h> 
#include <tchar.h> 
int _tmain() 
{ 
    char * pCmd = ::GetCommandLine(); 
    if (*pCmd++ == L'"')
    {
    	while (*pCmd++ != L'"'); 
    }
    else 
    {
    	while (*pCmd != NULL && *pCmd != L' ') 
    		++pCmd; 
    }

    while (*pCmd == L' ')
    	pCmd++; 

    STARTUPINFO si; 
    ZeroMemory( &si, sizeof(si) ); 
    si.cb = sizeof(si); 
    PROCESS_INFORMATION pi; 
    ZeroMemory(&pi, sizeof(pi)); 
    BOOL result = CreateProcess (NULL, pCmd, NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &si, &pi); 
    if (result) return 0; 

    char msg[2048]; 
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, ::GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_SYS_DEFAULT), msg, sizeof(msg), NULL); 
    fputs(msg, stderr); 
    _flushall(); 

    return -1; 
}