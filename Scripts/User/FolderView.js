﻿namespace("G").FolderView = function (rootElement) {
    var s3 = new AWS.S3();
    var me = this;
    var viewModal;
    var currentPath;
    var itemsTemplate;
    var currentFile;
    var thumbsOnScreen;
    var idUnsafeRegexp = /[^a-z0-9_\uC0-\uD6\uD8-\uF6\uF8-\u2FF\u370-\u37D\u37F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u10000-\uEFFFF\uB7\u0300-\u036F\u203F-\u2040]/ig;
    var pxToPx = function (px) {
        return Number(px.substring(0, px.length - 2));
    };
    rootElement.append('<div class="myitems"><div class="item"><img /></div></div>');
    var mockImg = rootElement.find('img');
    var mh = pxToPx(mockImg.css('max-height'));
    var mw = pxToPx(mockImg.css('max-width'));
    var ew = (rootElement.find('.item').outerWidth(true) + rootElement.find('.item').outerWidth(false)) / 2;
    var eh = (rootElement.find('.item').outerHeight(true) + rootElement.find('.item').outerHeight(false)) / 2;

    var showFile = function (file) {
        if (file) {
            if (!viewModal) viewModal = new G.ImageView(rootElement, me);
            viewModal.show(file);
        }
        rootElement.find('.myitems').removeClass('loading');
    };
    var q = [];
    var running = 0;
    var uid = 0;
    var queue = function (prom) {
        if (prom) {
            prom.uid = uid++;
            q.push(prom);
        }
        while (running < 5 && q.length > 0) {
            var next = q.splice(0, 1)[0];
            running++;
            next().then(function () {
                running--;
                queue(null);
            }, function () {
                running--;
                queue(null);
            });
        }
    };
    var makeThumb = function (img) {
        var newImage = new Image();
        newImage.crossOrigin = "Anonymous";
        img.unbind('error');
        img.after('<span class="glyphicon glyphicon-dashboard no-thumb" title="Generating thumbnail..."></span>');
        img.hide();
        queue(function () {
            return new Promise(function (resolve, reject) {
                img.next('.no-thumb').addClass('now');
                newImage.onload = function () {
                    var canvas = $('<canvas>')[0];
                    var targetAspect = mw / mh, sourceAspect = newImage.width / newImage.height, scale = 1;
                    if (targetAspect > sourceAspect)
                        scale = mw / newImage.width;
                    else
                        scale = mh / newImage.height;
                    canvas.width = newImage.width * scale;
                    canvas.height = newImage.height * scale;
                    var ctx = canvas.getContext('2d');
                    ctx.drawImage(newImage, 0, 0, newImage.width, newImage.height, 0, 0, canvas.width, canvas.height);
                    var imgData = canvas.toDataURL('image/jpeg', .6);
                    img.attr('src', imgData).show();
                    img.next('.no-thumb').remove();
                    imgData = imgData.substring(5);
                    var cs = imgData.indexOf(',');
                    var ctype = imgData.substring(0, cs).split(';');
                    imgData = imgData.substring(cs + 1);
                    var buf = new AWS.util.Buffer(imgData, ctype[1]);
                    s3.putObject({ Bucket: G.Config.Bucket, ContentEncoding: 'base64', ContentType: ctype[0], Key: img.data('thumbsrc'), Body: buf }, function (err, ok) {
                        if (err)
                            reject();
                        else
                            resolve();
                    });
                };
                newImage.onerror = function () {
                    img.next('.no-thumb').addClass('err glyphicon-remove').removeClass('glyphicon-dashboard');
                    reject();
                };
                newImage.src = s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: img.data('realsrc'), Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 });
            });
        });
    };

    var badCredentials = function () {
        G.TemplateManager.GetTemplate('Elements/BadCredentials.html').then(function (template) {
            rootElement.html(template);
            rootElement.find('.modal').modal();
        });
    };
    this.Prev = function (path) {
        while (path[path.length - 1] == '/') path = path.substring(0, path.length - 1);
        var ls = path.lastIndexOf('/');
        if (ls != -1)
            path = path.substring(ls + 1);
        var elem = rootElement.find('#' + path.replace(idUnsafeRegexp, '-'));
        if (elem.length)
            return elem.prev('.item').data('path');
        return null;
    };
    this.Next = function (path) {
        while (path[path.length - 1] == '/') path = path.substring(0, path.length - 1);
        var ls = path.lastIndexOf('/');
        if (ls != -1)
            path = path.substring(ls + 1);
        var elem = rootElement.find('#' + path.replace(idUnsafeRegexp, '-'));
        if (elem.length)
            return elem.next('.item').data('path');
        return null;
    };

    var prepareData = function (folderData) {
        folderData.CommonPrefixes.forEach(function (p) {
            p.Name = p.Prefix.split('/');
            for (var i = p.Name.length - 1; i >= 0; i--)
                if (p.Name[i]) {
                    p.Name = p.Name[i];
                    break;
                }
        });
        var realContents = [];
        folderData.Contents.forEach(function (p) {
            p.Name = p.Key.split('/');
            for (var i = p.Name.length - 1; i >= 0; i--)
                if (p.Name[i]) {
                    p.Name = p.Name[i];
                    break;
                }
            if (p.Name.substring(0, 6) == 'thumb_') {
                return;
            }
            var ls = p.Key.lastIndexOf('/');
            p.ThumbKey = p.Key.substring(0, ls + 1) + 'thumb_' + p.Key.substring(ls + 1);
            p.Src = s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: p.ThumbKey, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 });
            p.Id = p.Name.replace(idUnsafeRegexp, '-');
            if (p.Key == currentFile)
                p.Current = true;
            if (p.Size)
                realContents.push(p);
        });
        folderData.RealContents = realContents;
    };

    var renderElements = function (data) {
        prepareData(data);
        rootElement.find('.myitems').append($.mustache(itemsTemplate, data)).removeClass('loading');
    };
    var activateElements = function () {
        rootElement.find('.item img').each(function () {
            var img = $(this);
            if (!img.attr('src'))
                img.error(function () { makeThumb(img); }).attr('src', img.data('src'));
        });
    };
    var bindLoadMore = function (data) {
        if (data.IsTruncated) {
            var nextMarker = data.NextMarker || data.Contents[data.Contents.length - 1].Key;
            rootElement.find('.myitems').on('scroll', function (e) {
                var bottom = $(this).scrollTop() + $(this).height();
                var max = $(this).prop('scrollHeight');
                if (bottom > max - eh) {
                    $(this).unbind('scroll');
                    s3.Promise('listObjects', { Bucket: G.Config.Bucket, Prefix: currentPath, Delimiter: '/', MaxKeys: thumbsOnScreen, Marker: nextMarker })
                        .then(function (nextData) {
                            renderElements(nextData);
                            bindLoadMore(nextData);
                            activateElements();
                        });
                }
            }).trigger('scroll');
        }
    };

    this.Navigate = function (path, file) {
        if (path[0] == '/') path = path.substring(1);
        if (path || file)
            document.title = path + (file || '') + ' - AWS JS Gallery';
        else
            document.title = 'Home - AWS JS Gallery';
        if (currentPath != path) {
            currentFile = file;
            thumbsOnScreen = Math.floor($(window).width() / ew) * (1 + Math.ceil($(window).height() / eh));
            rootElement.find('.myitems').addClass('loading');
            Promise.all([
                    s3.Promise('listObjects', { Bucket: G.Config.Bucket, Prefix: path, Delimiter: '/', MaxKeys: thumbsOnScreen }),
                    G.TemplateManager.GetTemplate('Elements/FolderView.html')
            ])
                .then(function (data) {
                    if (data[0].Contents.length == 1 && data[0].Contents[0].Key == path && data[0].Contents[0].Size) {
                        me.Navigate(path.substring(0, path.lastIndexOf('/') + 1), path);
                    } else {
                        rootElement.find('.myitems').empty();
                        viewModal = null;
                        var viewData = data[0];
                        currentPath = path;

                        viewData.PathParts = [{ Path: '', Name: 'Home', Glyph: 'home' }];
                        if (path.length > 0) {
                            var cur = '';
                            while (path[path.length - 1] == '/') path = path.substring(0, path.length - 1);
                            viewData.PathParts = viewData.PathParts.concat(path.split('/').map(function (e) { return { Path: cur += e + '/', Name: e, Glyph: 'folder-close' }; }));
                        }
                        viewData.PathParts[viewData.PathParts.length - 1].Active = true;
                        viewData.PathParts[viewData.PathParts.length - 1].Glyph = 'folder-open';


                        data[1] = data[1].split('|||');
                        itemsTemplate = data[1][1];
                        rootElement.html($.mustache(data[1][0], viewData));

                        renderElements(data[0]);
                        showFile(file);
                        activateElements();

                        rootElement.find('.myitems').on('click', '.item', function (e) {
                            window.location = $(this).find('a').attr('href');
                        });
                        bindLoadMore(data[0]);
                    }
                }, badCredentials);
        } else showFile(file);
    };
};