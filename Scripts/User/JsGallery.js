﻿namespace("G").JsGallery = function(rootElement) {
    rootElement.addClass('loading');
    var username = store.get('username');
    var password = store.get('password');

    var attempt = false;
    var unathorized = function() {
        G.TemplateManager.GetTemplate('Elements/Login.html')
            .then(function(template) {
                if (attempt)
                    rootElement.find('.err').show();
                else {
                    rootElement.html(template);
                    rootElement.find('.modal').modal();
                }
                rootElement.find('form').on('submit', function (e) {
                    e.preventDefault();
                    attempt = true;

                    try {
                        tryLogin(rootElement.find('#username').val(), rootElement.find('#password').val(), rootElement.find('#persist-login').is(':checked'));
                    } catch (e) {
                    }
                    return false;
                });
            });
    };
    var tryLogin = function (username, password, persist) {
        if (!/[-+a-z0-9@_.]/i.test(username))
            username = '';
        if (persist) {
            store.set('username', username);
            store.set('password', password);
        } else if (persist === false) {
            store.set('username', null);
            store.set('password', null);
        }
        (username ? G.Utils.Get("Credentials/" + username + ".txt") : Promise.reject())
            .then(function(data) {
                var decrypted = CryptoJS.AES.decrypt(data, password).toString(CryptoJS.enc.Utf8);
                var ix = decrypted.indexOf('.');
                if (ix == -1)
                    throw 'Cannot parse credentials';
                haveCredentials(JSON.parse(decrypted.substring(ix + 1)));
            }, unathorized);
        return false;
    };

    var haveCredentials = function(creds) {
        AWS.config.update({ accessKeyId: creds.key, secretAccessKey: creds.secret });
        $('.modal').modal('hide');
        G.Config.Bucket = creds.bucket;
        var exp = new Date();
        exp = new Date(exp.getFullYear() + 2, 1, 1);
        G.Config.Expiration = exp;
        var view = new G.FolderView(rootElement);
        Satnav({ html5: false, force: true, poll: 100 })
            .navigate({
                path: '^$',
                directions: function(params) {
                    view.Navigate('/');
                }
            })
            .navigate({
                path: '^{*path}$',
                directions: function (params) {
                    view.Navigate(decodeURI(params.path));
                }
            })
            .go();
    };
    tryLogin(username, password);
};