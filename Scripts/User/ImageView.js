﻿namespace("G").ImageView = function(rootElement, folderView) {
    var me = this;
    var s3 = new AWS.S3();
    var dom;

    this.show = function(path) {
        Promise.resolve(dom || G.TemplateManager.GetTemplate('Elements/ImageView.html').then(function(tmpl) {
                rootElement.append(tmpl);
                dom = rootElement.find('.imageview');
                dom.on('click', '.command-close', function() {
                    dom.hide();
                    window.location = '#' + path.substring(0, path.lastIndexOf('/') + 1);
                }).on('click', '.command-prev,.command-next', function() {
                    window.location = '#' + $(this).data('link');
                });
                dom.find('.command-original').attr('href', s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: path, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 }));
                return dom;
            }))
            .then(function(modal) {
                modal.show();
            var contentTypePromise;
                if ((/\.(jpg|gif|png|jpeg|jp2k|jp2000|svg)$/i).test(path)) {
                    contentTypePromise = Promise.resolve('image/*');
                } else {
                    contentTypePromise = new Promise(function(resolve, reject) {
                        s3.headObject({ Bucket: G.Config.Bucket, Key: path }, function(err,data) {
                            if (err)reject(err);
                            resolve(data.ContentType);
                        });
                    });
                }
            contentTypePromise.then(function(contentType) {
                var url = s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: path, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 });
                var container = modal.find('.img').empty();
                dom.find('.command-original').attr('href', s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: path, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 }));
                if (/^image\//i.test(contentType)) {
                    var img = $('<img>');
                    container.append(img);
                    img.attr('src', '');
                    img.error(function(e) {
                        console.log(e);
                    });
                    img.attr('src', url);
                    dom.find('.prevoverlay,.nextoverlay').show();
                } else if (/^video\//i.test(contentType)) {
                    /*var video = $('<video>').attr('controls','controls');
                    video.append($('<source>').attr('src', url).attr('type', contentType));*/
                    var video = $('<embed>')
                        .attr('type', 'application/x-vlc-plugin')
                        .attr('autoplay', 'yes')
                        .attr('target', url)
                        .attr('type', contentType);
                    video.width(container.width());
                    video.height(container.height() - dom.find('.topoverlay').height());
                    video.css('margin-top', dom.find('.topoverlay').height());
                    container.append(video);
                    dom.find('.prevoverlay,.nextoverlay').hide();
                } else {
                    container.append('<span class="glyphicon glyphicon-remove"></span>');
                    dom.find('.prevoverlay,.nextoverlay').show();
                }
            });
                var prev = folderView.Prev(path);
                var next = folderView.Next(path);
                modal.find('.command-prev').toggle(prev != null).data('link', prev).attr('title', prev);
                modal.find('.command-next').toggle(next != null).data('link', next).attr('title', next);
            });
    };
};