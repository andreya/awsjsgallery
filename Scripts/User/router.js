﻿namespace('G').Router = Backbone.Router.extend({
    routes: {
    },
    initialize: function () {
        this.route('*path', this.path);
    },
    execute: function (callback, args) {
        callback.apply(this, args);
    },
    path: function (path) {
        G.JsGallery2.go(Backbone.history.getFragment());
    }
});
