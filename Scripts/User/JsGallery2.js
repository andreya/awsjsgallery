﻿namespace('G').JsGallery2 = function () {
    this.initialize.apply(this, arguments);
};

_.extend(G.JsGallery2.prototype, Backbone.Events, {
    initialize: function (options) {
        this.options = options;
        G.JsGallery2.Instance = this;
    },
    run: function () {
        var login = new G.LoginDialog();
        var that = this;
        this.listenTo(login, 'authorized', function() {
            that.setMainView(new G.FolderView());
        });
    },
    go:function(path) {
        
    },
    hideMainView: function () {
        if (this.__currentMainView) {
            this.__currentMainView.trigger('hide');
            if (this.__currentMainView.waitHideAnimation) {
                var dlg = this.__currentMainView;
                this.listenToOnce(this.__currentMainView, this.__currentMainView.waitHideAnimation, function () {
                    dlg.remove();
                });
            } else {
                this.__currentMainView.remove();
            }
            this.stopListening(this.__currentMainView);
            this.__currentMainView = null;
        }
    },
    setMainView: function (view) {
        this.hideMainView();
        this.options.content.empty().append(view.$el);
        view.trigger('show');
        this.setTitle(view.title);
        var that = this;
        this.listenTo(view, 'title', function () {
            that.setTitle(that.__currentMainView.title);
        });
        this.__currentMainView = view;
    },
    setTitle: function (newTitle) {
        document.title = (newTitle ? newTitle + ' - ' : "") + global.AdminApp.Loc.Misc.GlobalTitle;
        $('#nav-title').text(newTitle);
    },
    showDialog: function (dialogView) {
        var that = this;
        var p = this.hideDialog() || this.__lastDialogHide || Promise.resolve();
        p.then(function () {
            that.__currentDialog = dialogView;
            that.options.dialogs.empty().append(dialogView.$el);
            dialogView.trigger('show');
        });
    },
    hideDialog: function () {
        var p = null;
        var dlg = this.__currentDialog;
        if (dlg) {
            dlg.trigger('hide');
            if (dlg.waitHideAnimation) {
                var that = this;

                p = new Promise(function (resolve) {
                    that.listenToOnce(dlg, dlg.waitHideAnimation, function () {
                        dlg.remove();
                        resolve();
                        that.__lastDialogHide = null;
                    });
                });
                this.__lastDialogHide = p;
            } else {
                dlg.remove();
            }
            this.__currentDialog = null;
        }
        return p;
    },
    appNav: function (ev) {
        var target = $(ev.target).closest('a');
        if (target[0].tagName === 'A' && target.attr('href')) {
            target = target.attr('href');
        } else {
            target = target.data('href');
        }
        $('.tooltip.in,.live.in').removeClass('in');
        this.router.navigate(target, { trigger: true });
        return false;
    },
    setQuery: function (hash) {
        var q = $.param(hash);
        var frag = Backbone.history.getFragment();
        if (frag.indexOf('?') != -1) {
            if (q) {
                frag = frag.substring(0, frag.indexOf('?') + 1) + q;
            } else {
                frag = frag.substring(0, frag.indexOf('?'));
            }
        } else {
            if (q) {
                frag = frag + '?' + q;
            }
        }
        this.router.navigate(frag, { trigger: false });
    },
    getQuery: function () {
        var frag = Backbone.history.getFragment();
        if (frag.indexOf('?') != -1) {
            return _.deparam(frag.substring(frag.indexOf('?') + 1));
        } else {
            return {};
        }
    },
    updateQuery: function (hash) {
        var frag = Backbone.history.getFragment();
        if (frag.indexOf('?') != -1) {
            var existing = _.deparam(frag.substring(frag.indexOf('?') + 1));
            var newHash = $.param(_.extend(existing, hash));
            if (newHash) {
                frag = frag.substring(0, frag.indexOf('?') + 1) + newHash;
            } else {
                frag = frag.substring(0, frag.indexOf('?'));
            }
        } else {
            if ($.param(hash)) {
                frag = frag + '?' + $.param(hash);
            }
        }
        this.router.navigate(frag, { trigger: false });
    }
});
