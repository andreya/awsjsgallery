﻿namespace('G').Dialog = Backbone.View.extend({
    events: {
        'click .command-close': 'close',
        'show': 'setFocus'
    },
    close: function (ev) {
        global.AdminApp.hideDialog(ev);
    },
    setFocus: function () {
        var firstInput = this.$('input:first');
        if (firstInput.length) {
            firstInput[0].select();
        }
    }
});
G.Dialog.extend = Backbone.Model.extend;
namespace('G').BootstrapDialog = G.Dialog.extend({
    initialize: function () {
        this.on('show', this.showBootstrapModal);
        this.on('hide', this.hideBootstrapModal);
        Backbone.baseInit(this, G.Dialog);
    },
    waitHideAnimation: 'hideCompleted',
    showBootstrapModal: function () {
        var that = this;
        var hiding = false;
        this.$('.modal')
            .on('hide.bs.modal', function () {
                if (!hiding) {
                    hiding = true;
                    global.AdminApp.hideDialog();
                    hiding = false;
                }
            })
            .on('hidden.bs.modal', function () {
                that.trigger('hideCompleted');
            })
            .on('shown.bs.modal', function () {
                that.__backdrop = $('body>.modal-backdrop').last();
            })
            .modal('show');
        G.Dialog.prototype.setFocus.call(this);
    },
    hideBootstrapModal: function () {
        this.$('.modal').modal('hide');
    },
    remove: function () {
        if (this.__backdrop) {
            this.__backdrop.remove();
        }
    }
});
G.BootstrapDialog.extend = Backbone.Model.extend;
