﻿namespace('G').LoginDialog = G.BootstrapDialog.extend({
    events: {
        'submit form':'uiLogin'
    },
    initialize: function () {
        this.tryLogin(store.get('username'), store.get('password'));
        Backbone.baseInit(this, G.BootstrapDialog);
    },
    uiLogin:function(ev) {
        ev.preventDefault();
        this.tryLogin(this.$('#username').val(), this.$('#password').val(), this.$('#persist-login').is(':checked'));
    },
    tryLogin: function (username, password, persist) {
        var that = this;
        if (!/[-+a-z0-9@_.]/i.test(username))
            username = '';
        if (persist) {
            store.set('username', username);
            store.set('password', password);
        } else if (persist === false) {
            store.set('username', null);
            store.set('password', null);
        }
        (username ? G.Utils.Get("Credentials/" + username + ".txt") : Promise.reject())
            .then(function(data) {
                var decrypted = CryptoJS.AES.decrypt(data, password).toString(CryptoJS.enc.Utf8);
                var ix = decrypted.indexOf('.');
                if (ix == -1)
                    throw 'Cannot parse credentials';
                that.trigger('authorized', JSON.parse(decrypted.substring(ix + 1)));
            }, function() { that.showDialog() });
        return false;
    },
    showDialog:function() {
        var that = this;
        G.TemplateManager.GetTemplate('Elements/Login.html').then(function (template) {
            that.$el.html(template);
            G.JsGallery2.Instance.showDialog(that);
        });
    }
});