namespace("G.Utils").Post = function (url, data) {
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        req.open('POST', url);
        req.onload = function () {
            if (req.status == 200) {
                resolve(req.response);
            } else {
                reject(Error(req.statusText));
            }
        };
        req.onerror = function () {
            reject(Error("Network Error"));
        };
        req.send(data);
    });
};
namespace("G.Utils").Get = function (url) {
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        req.open('GET', url);
        req.onload = function () {
            if (req.status == 200) {
                resolve(req.response);
            } else {
                reject(Error(req.statusText));
            }
        };
        req.onerror = function () {
            reject(Error("Network Error"));
        };
        req.send();
    });
};

namespace("G.Utils").CallWebService = function (url, data) {
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        req.open('POST', url);
        //console.log('WS', url);
        req.setRequestHeader('Content-type', "application/json; charset=utf-8");
        if (G.Game&&G.Game.AccessToken && data.request && !data.request.Token) {
            data.request.Token = G.Game.AccessToken;
        }
        req.onload = function () {
            if (req.status == 200) {
                //console.log('OK', req.response);
                resolve(JSON.parse(req.response));
            } else {
                //console.log('FAIL', req);
                reject(Error(req.statusText));
            }
        };
        req.onerror = function () {
            reject(Error("Network Error"));
        };
        req.send(JSON.stringify(data));
    });
};
namespace("G.Utils").PromiseWebService = function(url, data) {
    return function() {
        return G.Utils.CallWebService(url, data);
    };
};
namespace("G.Utils").Success = function(data) {
    return new Promise(function (resolve, reject) {
        if (data && data.Success)
            resolve(data);
        else {
            reject(data);
        }
    });
};

namespace("G.Utils").CachedCallWebService = function(key, url, data) {
    return new Promise(function(resolve, reject) {
        if (!G.Utils.Cache)
            G.Utils.Cache = {};
        if (G.Utils.Cache[key]) {
            if (G.Utils.Cache[key].Request) {
                var old = G.Utils.Cache[key].Callback;
                G.Utils.Cache[key].Callback = function(rv) {
                    if (old)old(rv);
                    resolve(rv);
                };
            } else
                resolve(G.Utils.Cache[key].Data);
            return;
        }
        G.Utils.Cache[key] = {};
        G.Utils.Cache[key].Callback = resolve;
        G.Utils.Cache[key].ErrorCallback = reject;
        G.Utils.Cache[key].Request = G.Utils.CallWebService(url, data).then(function(d) {
            G.Utils.Cache[key].Request = null;
            G.Utils.Cache[key].Data = d;
            G.Utils.Cache[key].Callback(d);
        }, function() {
            G.Utils.Cache[key].Request = null;
            if (G.Utils.Cache[key].ErrorCallback)
                G.Utils.Cache[key].ErrorCallback.apply(arguments);
        });
    });
};
