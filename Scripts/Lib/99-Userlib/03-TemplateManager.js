﻿namespace("G.Utils").TemplateManager = function () {
    this.TemplateCache = {};
    this.Locale = '';
    var me = this;
    this.GetTemplate = function (templatePath) {
        return new Promise(function (resolve, reject) {
            var cached = me.TemplateCache[templatePath];
            if (cached) {
                resolve(cached);
            } else {
                Promise.resolve($.get(templatePath))
                    .then(function (data) {
                        me.TemplateCache[templatePath] = data;
                        resolve(data);
                    }, reject);
            }
        });
    };
    this.GetTemplates = function (templatePaths) {
        if (arguments.length > 1)
            templatePaths = Array.prototype.slice.call(arguments);
        return Promise.all(templatePaths.map(me.GetTemplate));
    };
};
G.TemplateManager = new G.Utils.TemplateManager();
