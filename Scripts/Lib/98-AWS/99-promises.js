﻿AWS.S3.prototype.Promise = function(fn, params) {
    if (typeof fn == 'string')
        fn = this[fn];
    var me = this;
    return new Promise(function(resolve, reject) {
        try {
            fn.apply(me, [
                params, function(err, data) {
                    if (err) reject(err);
                    resolve(data);
                }
            ]);
        } catch (e) {
            reject(e);
        }
    });
};