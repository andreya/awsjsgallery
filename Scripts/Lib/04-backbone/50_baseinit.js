﻿Backbone.baseInit = function (that, parentClass) {
    if (parentClass.extraExtends) {
        _.each(parentClass.extraExtends, function (e) { _.extend(that, e); });
    }
    _.extend(that.events, parentClass.prototype.events);
    var otherArgs = Array.prototype.slice.call(arguments, 2);
    parentClass.prototype.initialize.apply(that, otherArgs);
};