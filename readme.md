# What is it? #
Photo gallery written in javascript which can handle photos stored in Amazon AWS S3 bucket. Check live demo here:
http://andreya-gallery.s3-website-us-east-1.amazonaws.com/
To login use demo/demo user.

Features:

* Progressive loading - only one screen of thumbnails is loaded at once to help low-end computers digest the page.

* Persistent thumbnail generation - thumbs are generated on the fly and stored back to S3 bucket for future use.

* Encrypted credentials - you dont need to remember you AWS access key and secret, just regular username and password. Your credentials are encypted and not readable without your password.

* No server-side code - no hosting required! You can use Amazon S3 static site as hosting resulting in <$1 monthly hosting price (depends on you use).

* Touch friendly - no small links or screen-eating elements. No multitouch gestures too - just tap on left/right side of image to move to prev/next one.

# Building #
Everything is ready to deploy and does not require building, although you may want to build it yourself in case you modify the sources.
You can use Microsoft Visual Studio to build, but this is overkill, you dont really need it. Basically you'll need node.js 
installed on your computer, after that you just run in the project folder:
```
#!cmd
npm install -g grunt-cli
npm install
grunt
```
If you prefer minified JS, run 
```
#!cmd
grunt uglify
```


# Installing #
Deploy following files and folders to target server:

* Elements

* Resources

* Index.html

* MakeCredentials.html

* libs.js

* scripts.js

* styles.js

And you're done with installing! However you still need to store your AWS credentials, so navigate to the installed MakeCredentials.html,
 type in your desired username and password, your AWS access credentials (IAM user with restricted access is strongly recommended).
 After that copy resulting base64 string and paste it into <username>.txt file in the Credentials folder.

If you create IAM user for your gallery make sure you give him access to list your bucket contents and get objects. Additionally you should give him PutObject access to be able to generate persistent thumbnails. You can use the following bucket policy as a reference:
```
#!json
{
	"Version": "2008-10-17",
	"Id": "policy-demo-gallery",
	"Statement": [
		{
			"Sid": "stms-allow-get-put",
			"Effect": "Allow",
			"Principal": {
				"AWS": "*"
			},
			"Action": [
				"s3:GetObject",
				"s3:PutObject"
			],
			"Resource": "arn:aws:s3:::gallery-demo/*"
		},
		{
			"Sid": "stmt-allow-list",
			"Effect": "Allow",
			"Principal": {
				"AWS": "*"
			},
			"Action": "s3:ListBucket",
			"Resource": "arn:aws:s3:::gallery-demo"
		}
	]
}
```