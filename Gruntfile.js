module.exports = function(grunt) {
    var scripts = [
        {
            target: 'libs',
            destination: 'libs.js',
            sources: ["Scripts/Lib/**/*.js"]
        },
        {
            target: 'user',
            destination: 'scripts.js',
            sources: ["Scripts/User/**/*.js"]
        },
    ];
    var css = [
        {
            target: 'styles',
            destination: 'styles.css',
            sources: [
                "Styles/Bootstrap/bootstrap.less",
                "Styles/lib/*.less",
                "Styles/elements/*.less",
                "Styles/site.less"
            ]
        },
    ];

    var makebuild = function(src) {
        var rv = {};

        for (var i = 0; i < src.length; i++) {
            var f = {};

            f[src[i].destination] = src[i].sources;
            rv[src[i].target] = { files: f };
        }

        return rv;
    };

    var uglifyBuild = makebuild(scripts);

    uglifyBuild['options'] = { mangle: true, compress:{drop_console:true} };

    var makewatch = function(xtask, src, task) {
        for (var i = 0; i < src.length; i++) {
            xtask[task + '-' + src[i].target] = {
                files: src[i].files || src[i].sources,
                tasks: [task + ':' + src[i].target],
//                options: { nospawn: true }
            };
        }
    };

    var wtch = {};

    makewatch(wtch, scripts, 'concat');
    makewatch(wtch, css, 'less');
    grunt.initConfig({
        'pkg': grunt.file.readJSON('package.json'),
        'uglify': uglifyBuild,
	'concat': uglifyBuild,
        'less': makebuild(css),
        'watch': wtch
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('release', ['uglify', 'less']);
    grunt.registerTask('default', ['concat', 'less']);
    grunt.registerTask('watch_jg', ['watch']);
};
