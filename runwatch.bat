@echo off
if not exist node_modules\grunt cmd /c npm install
if X%1X==XReleaseX goto autobuild
if not X%JENKINS_URL%X == XX goto autobuild
wmic process where (name="node.exe") get commandline | findstr /i /c:"grunt" | findstr /i /c:"watch_jg"> nul
if %ERRORLEVEL% == 0 goto running
cmd /c grunt
runjob cmd /c grunt watch_jg
goto eof
:running
echo Watcher process already running
goto eof
:autobuild
echo This is auto build, forcing rebuild and not entering watch
cmd /c grunt release
:eof