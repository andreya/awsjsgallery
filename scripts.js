namespace('G').Dialog = Backbone.View.extend({
    events: {
        'click .command-close': 'close',
        'show': 'setFocus'
    },
    close: function (ev) {
        global.AdminApp.hideDialog(ev);
    },
    setFocus: function () {
        var firstInput = this.$('input:first');
        if (firstInput.length) {
            firstInput[0].select();
        }
    }
});
G.Dialog.extend = Backbone.Model.extend;
namespace('G').BootstrapDialog = G.Dialog.extend({
    initialize: function () {
        this.on('show', this.showBootstrapModal);
        this.on('hide', this.hideBootstrapModal);
        Backbone.baseInit(this, G.Dialog);
    },
    waitHideAnimation: 'hideCompleted',
    showBootstrapModal: function () {
        var that = this;
        var hiding = false;
        this.$('.modal')
            .on('hide.bs.modal', function () {
                if (!hiding) {
                    hiding = true;
                    global.AdminApp.hideDialog();
                    hiding = false;
                }
            })
            .on('hidden.bs.modal', function () {
                that.trigger('hideCompleted');
            })
            .on('shown.bs.modal', function () {
                that.__backdrop = $('body>.modal-backdrop').last();
            })
            .modal('show');
        G.Dialog.prototype.setFocus.call(this);
    },
    hideBootstrapModal: function () {
        this.$('.modal').modal('hide');
    },
    remove: function () {
        if (this.__backdrop) {
            this.__backdrop.remove();
        }
    }
});
G.BootstrapDialog.extend = Backbone.Model.extend;

namespace("G").FolderView = function (rootElement) {
    var s3 = new AWS.S3();
    var me = this;
    var viewModal;
    var currentPath;
    var itemsTemplate;
    var currentFile;
    var thumbsOnScreen;
    var idUnsafeRegexp = /[^a-z0-9_\uC0-\uD6\uD8-\uF6\uF8-\u2FF\u370-\u37D\u37F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u10000-\uEFFFF\uB7\u0300-\u036F\u203F-\u2040]/ig;
    var pxToPx = function (px) {
        return Number(px.substring(0, px.length - 2));
    };
    rootElement.append('<div class="myitems"><div class="item"><img /></div></div>');
    var mockImg = rootElement.find('img');
    var mh = pxToPx(mockImg.css('max-height'));
    var mw = pxToPx(mockImg.css('max-width'));
    var ew = (rootElement.find('.item').outerWidth(true) + rootElement.find('.item').outerWidth(false)) / 2;
    var eh = (rootElement.find('.item').outerHeight(true) + rootElement.find('.item').outerHeight(false)) / 2;

    var showFile = function (file) {
        if (file) {
            if (!viewModal) viewModal = new G.ImageView(rootElement, me);
            viewModal.show(file);
        }
        rootElement.find('.myitems').removeClass('loading');
    };
    var q = [];
    var running = 0;
    var uid = 0;
    var queue = function (prom) {
        if (prom) {
            prom.uid = uid++;
            q.push(prom);
        }
        while (running < 5 && q.length > 0) {
            var next = q.splice(0, 1)[0];
            running++;
            next().then(function () {
                running--;
                queue(null);
            }, function () {
                running--;
                queue(null);
            });
        }
    };
    var makeThumb = function (img) {
        var newImage = new Image();
        newImage.crossOrigin = "Anonymous";
        img.unbind('error');
        img.after('<span class="glyphicon glyphicon-dashboard no-thumb" title="Generating thumbnail..."></span>');
        img.hide();
        queue(function () {
            return new Promise(function (resolve, reject) {
                img.next('.no-thumb').addClass('now');
                newImage.onload = function () {
                    var canvas = $('<canvas>')[0];
                    var targetAspect = mw / mh, sourceAspect = newImage.width / newImage.height, scale = 1;
                    if (targetAspect > sourceAspect)
                        scale = mw / newImage.width;
                    else
                        scale = mh / newImage.height;
                    canvas.width = newImage.width * scale;
                    canvas.height = newImage.height * scale;
                    var ctx = canvas.getContext('2d');
                    ctx.drawImage(newImage, 0, 0, newImage.width, newImage.height, 0, 0, canvas.width, canvas.height);
                    var imgData = canvas.toDataURL('image/jpeg', .6);
                    img.attr('src', imgData).show();
                    img.next('.no-thumb').remove();
                    imgData = imgData.substring(5);
                    var cs = imgData.indexOf(',');
                    var ctype = imgData.substring(0, cs).split(';');
                    imgData = imgData.substring(cs + 1);
                    var buf = new AWS.util.Buffer(imgData, ctype[1]);
                    s3.putObject({ Bucket: G.Config.Bucket, ContentEncoding: 'base64', ContentType: ctype[0], Key: img.data('thumbsrc'), Body: buf }, function (err, ok) {
                        if (err)
                            reject();
                        else
                            resolve();
                    });
                };
                newImage.onerror = function () {
                    img.next('.no-thumb').addClass('err glyphicon-remove').removeClass('glyphicon-dashboard');
                    reject();
                };
                newImage.src = s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: img.data('realsrc'), Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 });
            });
        });
    };

    var badCredentials = function () {
        G.TemplateManager.GetTemplate('Elements/BadCredentials.html').then(function (template) {
            rootElement.html(template);
            rootElement.find('.modal').modal();
        });
    };
    this.Prev = function (path) {
        while (path[path.length - 1] == '/') path = path.substring(0, path.length - 1);
        var ls = path.lastIndexOf('/');
        if (ls != -1)
            path = path.substring(ls + 1);
        var elem = rootElement.find('#' + path.replace(idUnsafeRegexp, '-'));
        if (elem.length)
            return elem.prev('.item').data('path');
        return null;
    };
    this.Next = function (path) {
        while (path[path.length - 1] == '/') path = path.substring(0, path.length - 1);
        var ls = path.lastIndexOf('/');
        if (ls != -1)
            path = path.substring(ls + 1);
        var elem = rootElement.find('#' + path.replace(idUnsafeRegexp, '-'));
        if (elem.length)
            return elem.next('.item').data('path');
        return null;
    };

    var prepareData = function (folderData) {
        folderData.CommonPrefixes.forEach(function (p) {
            p.Name = p.Prefix.split('/');
            for (var i = p.Name.length - 1; i >= 0; i--)
                if (p.Name[i]) {
                    p.Name = p.Name[i];
                    break;
                }
        });
        var realContents = [];
        folderData.Contents.forEach(function (p) {
            p.Name = p.Key.split('/');
            for (var i = p.Name.length - 1; i >= 0; i--)
                if (p.Name[i]) {
                    p.Name = p.Name[i];
                    break;
                }
            if (p.Name.substring(0, 6) == 'thumb_') {
                return;
            }
            var ls = p.Key.lastIndexOf('/');
            p.ThumbKey = p.Key.substring(0, ls + 1) + 'thumb_' + p.Key.substring(ls + 1);
            p.Src = s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: p.ThumbKey, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 });
            p.Id = p.Name.replace(idUnsafeRegexp, '-');
            if (p.Key == currentFile)
                p.Current = true;
            if (p.Size)
                realContents.push(p);
        });
        folderData.RealContents = realContents;
    };

    var renderElements = function (data) {
        prepareData(data);
        rootElement.find('.myitems').append($.mustache(itemsTemplate, data)).removeClass('loading');
    };
    var activateElements = function () {
        rootElement.find('.item img').each(function () {
            var img = $(this);
            if (!img.attr('src'))
                img.error(function () { makeThumb(img); }).attr('src', img.data('src'));
        });
    };
    var bindLoadMore = function (data) {
        if (data.IsTruncated) {
            var nextMarker = data.NextMarker || data.Contents[data.Contents.length - 1].Key;
            rootElement.find('.myitems').on('scroll', function (e) {
                var bottom = $(this).scrollTop() + $(this).height();
                var max = $(this).prop('scrollHeight');
                if (bottom > max - eh) {
                    $(this).unbind('scroll');
                    s3.Promise('listObjects', { Bucket: G.Config.Bucket, Prefix: currentPath, Delimiter: '/', MaxKeys: thumbsOnScreen, Marker: nextMarker })
                        .then(function (nextData) {
                            renderElements(nextData);
                            bindLoadMore(nextData);
                            activateElements();
                        });
                }
            }).trigger('scroll');
        }
    };

    this.Navigate = function (path, file) {
        if (path[0] == '/') path = path.substring(1);
        if (path || file)
            document.title = path + (file || '') + ' - AWS JS Gallery';
        else
            document.title = 'Home - AWS JS Gallery';
        if (currentPath != path) {
            currentFile = file;
            thumbsOnScreen = Math.floor($(window).width() / ew) * (1 + Math.ceil($(window).height() / eh));
            rootElement.find('.myitems').addClass('loading');
            Promise.all([
                    s3.Promise('listObjects', { Bucket: G.Config.Bucket, Prefix: path, Delimiter: '/', MaxKeys: thumbsOnScreen }),
                    G.TemplateManager.GetTemplate('Elements/FolderView.html')
            ])
                .then(function (data) {
                    if (data[0].Contents.length == 1 && data[0].Contents[0].Key == path && data[0].Contents[0].Size) {
                        me.Navigate(path.substring(0, path.lastIndexOf('/') + 1), path);
                    } else {
                        rootElement.find('.myitems').empty();
                        viewModal = null;
                        var viewData = data[0];
                        currentPath = path;

                        viewData.PathParts = [{ Path: '', Name: 'Home', Glyph: 'home' }];
                        if (path.length > 0) {
                            var cur = '';
                            while (path[path.length - 1] == '/') path = path.substring(0, path.length - 1);
                            viewData.PathParts = viewData.PathParts.concat(path.split('/').map(function (e) { return { Path: cur += e + '/', Name: e, Glyph: 'folder-close' }; }));
                        }
                        viewData.PathParts[viewData.PathParts.length - 1].Active = true;
                        viewData.PathParts[viewData.PathParts.length - 1].Glyph = 'folder-open';


                        data[1] = data[1].split('|||');
                        itemsTemplate = data[1][1];
                        rootElement.html($.mustache(data[1][0], viewData));

                        renderElements(data[0]);
                        showFile(file);
                        activateElements();

                        rootElement.find('.myitems').on('click', '.item', function (e) {
                            window.location = $(this).find('a').attr('href');
                        });
                        bindLoadMore(data[0]);
                    }
                }, badCredentials);
        } else showFile(file);
    };
};
namespace("G").ImageView = function(rootElement, folderView) {
    var me = this;
    var s3 = new AWS.S3();
    var dom;

    this.show = function(path) {
        Promise.resolve(dom || G.TemplateManager.GetTemplate('Elements/ImageView.html').then(function(tmpl) {
                rootElement.append(tmpl);
                dom = rootElement.find('.imageview');
                dom.on('click', '.command-close', function() {
                    dom.hide();
                    window.location = '#' + path.substring(0, path.lastIndexOf('/') + 1);
                }).on('click', '.command-prev,.command-next', function() {
                    window.location = '#' + $(this).data('link');
                });
                dom.find('.command-original').attr('href', s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: path, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 }));
                return dom;
            }))
            .then(function(modal) {
                modal.show();
            var contentTypePromise;
                if ((/\.(jpg|gif|png|jpeg|jp2k|jp2000|svg)$/i).test(path)) {
                    contentTypePromise = Promise.resolve('image/*');
                } else {
                    contentTypePromise = new Promise(function(resolve, reject) {
                        s3.headObject({ Bucket: G.Config.Bucket, Key: path }, function(err,data) {
                            if (err)reject(err);
                            resolve(data.ContentType);
                        });
                    });
                }
            contentTypePromise.then(function(contentType) {
                var url = s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: path, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 });
                var container = modal.find('.img').empty();
                dom.find('.command-original').attr('href', s3.getSignedUrl('getObject', { Bucket: G.Config.Bucket, Key: path, Expires: (G.Config.Expiration.getTime() - (new Date().getTime())) / 1000 }));
                if (/^image\//i.test(contentType)) {
                    var img = $('<img>');
                    container.append(img);
                    img.attr('src', '');
                    img.error(function(e) {
                        console.log(e);
                    });
                    img.attr('src', url);
                    dom.find('.prevoverlay,.nextoverlay').show();
                } else if (/^video\//i.test(contentType)) {
                    /*var video = $('<video>').attr('controls','controls');
                    video.append($('<source>').attr('src', url).attr('type', contentType));*/
                    var video = $('<embed>')
                        .attr('type', 'application/x-vlc-plugin')
                        .attr('autoplay', 'yes')
                        .attr('target', url)
                        .attr('type', contentType);
                    video.width(container.width());
                    video.height(container.height() - dom.find('.topoverlay').height());
                    video.css('margin-top', dom.find('.topoverlay').height());
                    container.append(video);
                    dom.find('.prevoverlay,.nextoverlay').hide();
                } else {
                    container.append('<span class="glyphicon glyphicon-remove"></span>');
                    dom.find('.prevoverlay,.nextoverlay').show();
                }
            });
                var prev = folderView.Prev(path);
                var next = folderView.Next(path);
                modal.find('.command-prev').toggle(prev != null).data('link', prev).attr('title', prev);
                modal.find('.command-next').toggle(next != null).data('link', next).attr('title', next);
            });
    };
};
namespace("G").JsGallery = function(rootElement) {
    rootElement.addClass('loading');
    var username = store.get('username');
    var password = store.get('password');

    var attempt = false;
    var unathorized = function() {
        G.TemplateManager.GetTemplate('Elements/Login.html')
            .then(function(template) {
                if (attempt)
                    rootElement.find('.err').show();
                else {
                    rootElement.html(template);
                    rootElement.find('.modal').modal();
                }
                rootElement.find('form').on('submit', function (e) {
                    e.preventDefault();
                    attempt = true;

                    try {
                        tryLogin(rootElement.find('#username').val(), rootElement.find('#password').val(), rootElement.find('#persist-login').is(':checked'));
                    } catch (e) {
                    }
                    return false;
                });
            });
    };
    var tryLogin = function (username, password, persist) {
        if (!/[-+a-z0-9@_.]/i.test(username))
            username = '';
        if (persist) {
            store.set('username', username);
            store.set('password', password);
        } else if (persist === false) {
            store.set('username', null);
            store.set('password', null);
        }
        (username ? G.Utils.Get("Credentials/" + username + ".txt") : Promise.reject())
            .then(function(data) {
                var decrypted = CryptoJS.AES.decrypt(data, password).toString(CryptoJS.enc.Utf8);
                var ix = decrypted.indexOf('.');
                if (ix == -1)
                    throw 'Cannot parse credentials';
                haveCredentials(JSON.parse(decrypted.substring(ix + 1)));
            }, unathorized);
        return false;
    };

    var haveCredentials = function(creds) {
        AWS.config.update({ accessKeyId: creds.key, secretAccessKey: creds.secret });
        $('.modal').modal('hide');
        G.Config.Bucket = creds.bucket;
        var exp = new Date();
        exp = new Date(exp.getFullYear() + 2, 1, 1);
        G.Config.Expiration = exp;
        var view = new G.FolderView(rootElement);
        Satnav({ html5: false, force: true, poll: 100 })
            .navigate({
                path: '^$',
                directions: function(params) {
                    view.Navigate('/');
                }
            })
            .navigate({
                path: '^{*path}$',
                directions: function (params) {
                    view.Navigate(decodeURI(params.path));
                }
            })
            .go();
    };
    tryLogin(username, password);
};
namespace('G').JsGallery2 = function () {
    this.initialize.apply(this, arguments);
};

_.extend(G.JsGallery2.prototype, Backbone.Events, {
    initialize: function (options) {
        this.options = options;
        G.JsGallery2.Instance = this;
    },
    run: function () {
        var login = new G.LoginDialog();
        var that = this;
        this.listenTo(login, 'authorized', function() {
            that.setMainView(new G.FolderView());
        });
    },
    go:function(path) {
        
    },
    hideMainView: function () {
        if (this.__currentMainView) {
            this.__currentMainView.trigger('hide');
            if (this.__currentMainView.waitHideAnimation) {
                var dlg = this.__currentMainView;
                this.listenToOnce(this.__currentMainView, this.__currentMainView.waitHideAnimation, function () {
                    dlg.remove();
                });
            } else {
                this.__currentMainView.remove();
            }
            this.stopListening(this.__currentMainView);
            this.__currentMainView = null;
        }
    },
    setMainView: function (view) {
        this.hideMainView();
        this.options.content.empty().append(view.$el);
        view.trigger('show');
        this.setTitle(view.title);
        var that = this;
        this.listenTo(view, 'title', function () {
            that.setTitle(that.__currentMainView.title);
        });
        this.__currentMainView = view;
    },
    setTitle: function (newTitle) {
        document.title = (newTitle ? newTitle + ' - ' : "") + global.AdminApp.Loc.Misc.GlobalTitle;
        $('#nav-title').text(newTitle);
    },
    showDialog: function (dialogView) {
        var that = this;
        var p = this.hideDialog() || this.__lastDialogHide || Promise.resolve();
        p.then(function () {
            that.__currentDialog = dialogView;
            that.options.dialogs.empty().append(dialogView.$el);
            dialogView.trigger('show');
        });
    },
    hideDialog: function () {
        var p = null;
        var dlg = this.__currentDialog;
        if (dlg) {
            dlg.trigger('hide');
            if (dlg.waitHideAnimation) {
                var that = this;

                p = new Promise(function (resolve) {
                    that.listenToOnce(dlg, dlg.waitHideAnimation, function () {
                        dlg.remove();
                        resolve();
                        that.__lastDialogHide = null;
                    });
                });
                this.__lastDialogHide = p;
            } else {
                dlg.remove();
            }
            this.__currentDialog = null;
        }
        return p;
    },
    appNav: function (ev) {
        var target = $(ev.target).closest('a');
        if (target[0].tagName === 'A' && target.attr('href')) {
            target = target.attr('href');
        } else {
            target = target.data('href');
        }
        $('.tooltip.in,.live.in').removeClass('in');
        this.router.navigate(target, { trigger: true });
        return false;
    },
    setQuery: function (hash) {
        var q = $.param(hash);
        var frag = Backbone.history.getFragment();
        if (frag.indexOf('?') != -1) {
            if (q) {
                frag = frag.substring(0, frag.indexOf('?') + 1) + q;
            } else {
                frag = frag.substring(0, frag.indexOf('?'));
            }
        } else {
            if (q) {
                frag = frag + '?' + q;
            }
        }
        this.router.navigate(frag, { trigger: false });
    },
    getQuery: function () {
        var frag = Backbone.history.getFragment();
        if (frag.indexOf('?') != -1) {
            return _.deparam(frag.substring(frag.indexOf('?') + 1));
        } else {
            return {};
        }
    },
    updateQuery: function (hash) {
        var frag = Backbone.history.getFragment();
        if (frag.indexOf('?') != -1) {
            var existing = _.deparam(frag.substring(frag.indexOf('?') + 1));
            var newHash = $.param(_.extend(existing, hash));
            if (newHash) {
                frag = frag.substring(0, frag.indexOf('?') + 1) + newHash;
            } else {
                frag = frag.substring(0, frag.indexOf('?'));
            }
        } else {
            if ($.param(hash)) {
                frag = frag + '?' + $.param(hash);
            }
        }
        this.router.navigate(frag, { trigger: false });
    }
});

namespace('G').LoginDialog = G.BootstrapDialog.extend({
    events: {
        'submit form':'uiLogin'
    },
    initialize: function () {
        this.tryLogin(store.get('username'), store.get('password'));
        Backbone.baseInit(this, G.BootstrapDialog);
    },
    uiLogin:function(ev) {
        ev.preventDefault();
        this.tryLogin(this.$('#username').val(), this.$('#password').val(), this.$('#persist-login').is(':checked'));
    },
    tryLogin: function (username, password, persist) {
        var that = this;
        if (!/[-+a-z0-9@_.]/i.test(username))
            username = '';
        if (persist) {
            store.set('username', username);
            store.set('password', password);
        } else if (persist === false) {
            store.set('username', null);
            store.set('password', null);
        }
        (username ? G.Utils.Get("Credentials/" + username + ".txt") : Promise.reject())
            .then(function(data) {
                var decrypted = CryptoJS.AES.decrypt(data, password).toString(CryptoJS.enc.Utf8);
                var ix = decrypted.indexOf('.');
                if (ix == -1)
                    throw 'Cannot parse credentials';
                that.trigger('authorized', JSON.parse(decrypted.substring(ix + 1)));
            }, function() { that.showDialog() });
        return false;
    },
    showDialog:function() {
        var that = this;
        G.TemplateManager.GetTemplate('Elements/Login.html').then(function (template) {
            that.$el.html(template);
            G.JsGallery2.Instance.showDialog(that);
        });
    }
});
namespace('G').Router = Backbone.Router.extend({
    routes: {
    },
    initialize: function () {
        this.route('*path', this.path);
    },
    execute: function (callback, args) {
        callback.apply(this, args);
    },
    path: function (path) {
        G.JsGallery2.go(Backbone.history.getFragment());
    }
});
